# SoC Conception Lab folder 

Welcome to the SoC conception Lab folder. This folder contains all the source material for the different lab 
- The initiation Lab which describe Vivado environment and setup the Zybo PL 
- The embedded Lab (Lab1 to Lab4) which creates a full SoC environnment 
- The Video processing lab, which contains all the material to compute a real time video analysis


## Getting the source 

A local copy of the following folder can be downloaded under Linux with the following command 

        git clone https://gitlab.inria.fr/rgerzagu/3a_sysnum_soc_labs.git 
        
 
## Setting up environment 

This folder will be located on $TP and all the work should be launched from this directory.
When working on the Lab, the following step should be done 

- Launch a terminal and go to the cloned folder location (i.e $TP) :warning: you should absolutely be here 
- Check that everything is OK by typing `ls`. You should have 



		README.md             TP2_Partitionning/    zybo-z7-10/
		TP0_InitiationVivado/ TP3_RealTimeVideo/    ./launch_vivado.sh
		TP1_EmbeddedSystem/   TP4_Advanced_FIR/     ./Vivado_init.tcl

        
- Launch Vivado using the proposed command. Be sure that this is this command that it used as it also initialized the board repository to be able to use Zybo boards on Vivado 2017.4 
        
        ./launch_vivado.sh
        
- Do the lab :) 
        
